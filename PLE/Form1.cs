﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace PLE
{
    public partial class Form1 : Form
    {
        //initialization
        Bitmap OutputBitmap = new Bitmap(640, 480);
        Canvass MyCanvass;
        Boolean IfEx = false; //used to determine if we can execute the parsed if statement
        int var = 0; //variable holder
        Boolean FormExcep = false; //verify inputted text is usable
        int loopHolder = 0; //holds the amount if times I will loop the commands
        int endLoopLineNumber = 0; //remembers the line number for the end of the loop to jump to after looping
        int endMethodLineNumber = 0; //remebers the end of method line number to jump to 
        int radiusHolder = 0;   // a variable that can be set by the user to change circle radius
        int widthHolder = 0;// a variable that can be set by the user to change square/rectangel width
        int heightHolder = 0;// a variable that can be set by the user to change square/rectangle height
        int methodStart = 0;//a variable used to determine the start of the method
        String methodName;  // remebers the name of the method so that it can be called at anytime
        string[] commandArray;//holds all the commands from the method set by the user

        public Form1()
        {
            InitializeComponent();
            MyCanvass = new Canvass(Graphics.FromImage(OutputBitmap));
            MyCanvass.Clear(); //Refreshes the canvass to start with correct values and color
            Refresh();
        }

        private void commandLine_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter) //only reacts to enter input on commandline
                {
                    String Command = commandLine.Text.Trim(); //removes excess spaces from the ends ONLY
                    string[] split = Command.Split(' '); // splits by space characters for invidual components
                    if (split[0].Equals("drawTo") == true) //enters to drawTo command
                    {
                        int num1 = Convert.ToInt32(split[1]);   //converts initial string value of number to an int
                        int num2 = Convert.ToInt32(split[2]);
                        if ((num1 > 0 && num1 < 640) && (num2 > 0 && num2 < 640)) //makes sure the numbers are appropriate for the size of the canvass 0 - 640
                        {
                            MyCanvass.DrawLine(num1, num2); //draws a line from the held xPos/yPos in canvass class to two coordinates(num1, num2)
                            Console.WriteLine("LINE"); //displays in console for clarification
                        }
                        else
                        {
                            MyCanvass.PrintError(6, 0); //enters this else if the numbers entered are too big for the canvass
                        }

                    }
                    else if (split[0].Equals("square") == true)
                    {
                        int num1 = Convert.ToInt32(split[1]);   //converts initial string value of number to an int
                        if (num1 > 0 && num1 < 640) //makes sure the numbers are appropriate for the size of the canvass 0 - 640
                        {
                            MyCanvass.DrawSquare(num1); //draws a square around the held xPos/yPos in canvass class by side length: num1
                            Console.WriteLine("SQUARE");//displays in console for clarification   
                        }
                        else
                        {
                            MyCanvass.PrintError(6, 0);//enters this else if the numbers entered are too big for the canvass
                        }
                    }
                    else if (split[0].Equals("rectangle") == true)
                    {
                        int num1 = Convert.ToInt32(split[1]);   //converts initial string value of number to an int
                        int num2 = Convert.ToInt32(split[2]);
                        ;
                        if ((num1 > 0 && num1 < 640) && (num2 > 0 && num2 < 640))   //makes sure the numbers are appropriate for the size of the canvass 0 - 640
                        {
                            MyCanvass.DrawRectangle(num1, num2); //draws rectangle width, height around xPos yPos
                            Console.WriteLine("RECTANGLE");
                        }
                        else
                        {
                            MyCanvass.PrintError(6, 0);//number out of canvass range
                        }
                    }
                    else if (split[0].Equals("circle") == true)
                    {
                        int num1 = Convert.ToInt32(split[1]); //converts initial string value of number to an int

                        if (num1 > 0 && num1 < 640) //makes sure the numbers are appropriate for the size of the canvass 0 - 640
                        {
                            MyCanvass.DrawCircle(num1); //draws circle with radius - num1-  around xPos yPos
                            Console.WriteLine("CIRCLE");
                        }
                        else
                        {
                            MyCanvass.PrintError(6, 0); //number out of canvass range
                        }
                    }
                    else if (split[0].Equals("moveTo") == true) //moves pen without drawing
                    {
                        int num1 = Convert.ToInt32(split[1]);
                        int num2 = Convert.ToInt32(split[2]);

                        if ((num1 > 0 && num1 < 640) && (num2 > 0 && num2 < 640)) //numbers in range of canvass
                        {
                            MyCanvass.moveTo(num1, num2); //moves pen to coords
                            Console.WriteLine("Pen Moved");
                        }
                        else
                        {
                            MyCanvass.PrintError(6, 0); // error for out of range 
                        }
                    }
                    else if (split[0].Equals("clear") == true) //same as clear button resets to blank canvass
                    {
                        MyCanvass.Clear();
                    }
                    else if (split[0].Equals("triangle") == true)
                    {
                        int num1 = Convert.ToInt32(split[1]);

                        if (num1 > 0 && num1 < 640) //canvass range
                        {
                            MyCanvass.DrawTriangle(num1); //draws triangle of size - num1 - around xpos yPos
                            Console.WriteLine("TRIANGLE");
                        }
                        else
                        {
                            MyCanvass.PrintError(6, 0); //canvass range error
                        }
                    }
                    else if (split[0].Equals("reset") == true) //sets pen coords to 0,0. doesnt clear canvass
                    {
                        MyCanvass.moveTo(0, 0);
                        Console.WriteLine("Pen Moved");
                    }
                    else if (split[0].Equals("pen") == true) //changes pen color to 1 0f 4 colors
                    {
                        String color = split[1];
                        if (color == "red" || color == "green" || color == "yellow" || color == "black")
                        {
                            MyCanvass.penColor(color);
                            Console.WriteLine("PEN COLOR CHANGED TO: ", color);
                        }
                        else
                        {
                            MyCanvass.PrintError(3, 0); //error if they type color that is not an option
                        }
                    }
                    else if (split.Length >= 4) //checks amount of inputs
                    {
                        MyCanvass.PrintError(0, 0);//Too many parameters 
                    }
                    else if (split[0].Equals("fill") == true) //allows for the user to have the fill option on or off (y/n)
                    {
                        if (split[1].Equals("y") == true)
                        {
                            MyCanvass.fillYN(true);
                        }
                        else if (split[1].Equals("n") == true)
                        {
                            MyCanvass.fillYN(false);
                        }
                        else
                        {
                            MyCanvass.PrintError(4, 0); //error for not a valid fill option
                        }
                    }
                    else if (split[0].Equals("run") == true) //allows run to be typed in command line or pressed as a button
                    {
                        run();
                    }
                    else if (split[0].Equals("var") == true)   //allows for the user to have the fill option on or off (y/n)
                    {
                        var = Convert.ToInt32(split[1]);
                    }
                    else
                    {
                        MyCanvass.PrintError(2, 0);//incorrect commnad
                    }
                    commandLine.Text = "";
                    Refresh();
                }
            }
            catch (System.FormatException)// catches number format exception e.g float or letters
            {
                MyCanvass.PrintError(5, 0);
                Refresh();
            }
            catch (System.IndexOutOfRangeException)// catches number format exception e.g float or letters
            {
                MyCanvass.PrintError(7, 0);
                Refresh();
            }
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)//graphics
        {
            Graphics g = e.Graphics;
            g.DrawImageUnscaled(OutputBitmap, 0, 0);
        }

        private void run_Click(object sender, EventArgs e)
        {
            run();
        }
        public void parseCommand(String Line, int lNum)//run() passes commands here with their line number
        {
            try
            {
                String Command = Line.Trim(); //removes excess spaces from the ends ONLY
                string[] split = Command.Split(' ');    // splits by space characters for invidual components
                if (split[0].Equals("drawTo") == true)  //enters to drawTo command
                {
                    int num1 = Convert.ToInt32(split[1]);   //converts initial string value of number to an int
                    int num2 = Convert.ToInt32(split[2]);

                    if ((num1 > 0 && num1 < 640) && (num2 > 0 && num2 < 640))   //makes sure the numbers are appropriate for the size of the canvass 0 - 640
                    {
                        MyCanvass.DrawLine(num1, num2); //draws a line from the held xPos/yPos in canvass class to two coordinates(num1, num2)
                        Console.WriteLine("LINE");  //displays in console for clarification
                    }
                    else
                    {
                        //MyCanvass.PrintError(6, lNum); not displayed because of syntax button
                    }
                }
                else if (split[0].Equals("square") == true)
                {
                    int num1 = Convert.ToInt32(split[1]);   //converts initial string value of number to an int
                    if (num1 > 0 && num1 < 640) //makes sure the numbers are appropriate for the size of the canvass 0 - 640
                    {
                        MyCanvass.DrawSquare(num1); //draws a square around the held xPos/yPos in canvass class by side length: num1
                        Console.WriteLine("SQUARE");    //displays in console for clarification 
                    }
                    else
                    {
                        // MyCanvass.PrintError(6, 0);
                    }

                }

                else if (split[0].Equals("rectangle") == true)
                {
                    if (split.Length != 1)
                    {
                        int num1 = Convert.ToInt32(split[1]);    //converts initial string value of number to an int
                        int num2 = Convert.ToInt32(split[2]);

                        if ((num1 > 0 && num1 < 640) && (num2 > 0 && num2 < 640))   //makes sure the numbers are appropriate for the size of the canvass 0 - 640
                        {
                            MyCanvass.DrawRectangle(num1, num2);    //draws rectangle width, height around xPos yPos
                            Console.WriteLine("RECTANGLE");
                        }
                        else
                        {
                            //MyCanvass.PrintError(6, lNum);
                        }
                    }
                    else
                    {
                        if (widthHolder == 0 || heightHolder == 0)
                        {
                            Console.WriteLine("Error! need to preset height and width for rectangle - currently 0"); //implement error
                        }
                        else
                        {
                            MyCanvass.DrawRectangle(widthHolder, heightHolder);    //draws rectangle width, height around xPos yPos
                            Console.WriteLine("RECTANGLE");
                        }
                    }
                }
                else if (split[0].Equals("circle") == true)
                {
                    if (split[1].Equals("var") == true) //can draw circles with a variable set before hand
                    {
                        if (var == 0)
                        {
                            Console.WriteLine("Error! need to preset var - currently 0"); //if not set, show error
                        }
                        else
                        {
                            MyCanvass.DrawCircle(var);
                        }                        
                    }
                    else if (split[1].Equals("radius") == true) //draws circle with radius variable set before hand
                    {
                        if (radiusHolder == 0)
                        {
                            Console.WriteLine("Error! need to preset radius for circle - currently 0"); //if not set, show error
                        }
                        else
                        {
                            MyCanvass.DrawCircle(radiusHolder);
                        }
                    }
                    else
                    {
                        int num1 = Convert.ToInt32(split[1]);   //converts initial string value of number to an int

                        if (num1 > 0 && num1 < 640) //makes sure the numbers are appropriate for the size of the canvass 0 - 640
                        {
                            MyCanvass.DrawCircle(num1);  //draws circle with radius - num1-  around xPos yPos
                            Console.WriteLine("CIRCLE");
                        }
                        else
                        {
                            //MyCanvass.PrintError(6, 0);
                        }
                    }
                }
                else if (split[0].Equals("triangle") == true)
                {
                    if (split[1].Equals("var") == true) //can draw triangle with a variable set before hand
                    {
                        if (var == 0)
                        {
                            Console.WriteLine("Error! need to preset var - currently 0"); //if not set, show error
                        }
                        else
                        {
                            MyCanvass.DrawTriangle(var);
                        }
                    }
                    else
                    {
                        int num1 = Convert.ToInt32(split[1]);

                        if (num1 > 0 && num1 < 640) //canvass range
                        {
                            MyCanvass.DrawTriangle(num1);    //draws triangle of size - num1 - around xpos yPos
                            Console.WriteLine("TRIANGLE");
                        }
                        else
                        {
                            //MyCanvass.PrintError(6, 0);
                        }
                    }
                }
                else if (split[0].Equals("moveTo") == true) //moves pen without drawing
                {
                    int num1 = Convert.ToInt32(split[1]);
                    int num2 = Convert.ToInt32(split[2]);

                    if ((num1 > 0 && num1 < 640) && (num2 > 0 && num2 < 640))   //numbers in range of canvass
                    {
                        MyCanvass.moveTo(num1, num2);   //moves pen to coords
                        Console.WriteLine("Pen Moved");
                    }
                    else
                    {
                        //MyCanvass.PrintError(6, lNum);
                    }
                }
                else if (split[0].Equals("clear") == true)  //same as clear button resets to blank canvass
                {
                    MyCanvass.Clear();
                }
                else if (split[0].Equals("reset") == true)  //sets pen coords to 0,0. doesnt clear canvass
                {
                    MyCanvass.moveTo(0, 0);
                    Console.WriteLine("Pen Moved");
                }
                else if (split[0].Equals("pen") == true)    //changes pen color to 1 0f 4 colors
                {
                    String color = split[1];
                    if (color == "red" || color == "green" || color == "yellow" || color == "black")
                    {
                        MyCanvass.penColor(color);
                        Console.WriteLine("PEN COLOR CHANGED TO: ", color);
                    }
                    else
                    {
                        //MyCanvass.PrintError(3, lNum);
                    }
                }
                else if (split.Length >= 6) //checks amount of inputs
                {
                    //MyCanvass.PrintError(0, lNum);//Too many parameters 
                }
                else if (split[0].Equals("fill") == true)   //allows for the user to have the fill option on or off (y/n)
                {
                    if (split[1].Equals("y") == true)
                    {
                        MyCanvass.fillYN(true);
                    }
                    else if (split[1].Equals("n") == true)
                    {
                        MyCanvass.fillYN(false);
                    }
                    else
                    {
                        //MyCanvass.PrintError(4, 0);
                    }
                }
                else if (split[0].Equals("var") == true)   //allows for the user to have the fill option on or off (y/n)
                {
                    if (split.Length == 2)
                    {
                        var = Convert.ToInt32(split[1]);
                    }
                    else
                    {
                        int varInc = Convert.ToInt32(split[4]);
                        var = var + varInc;
                    }
                }
                else if (split[0].Equals("radius") == true)  //creates radius variable for a user to sue
                {
                    if (split.Length == 2)
                    {
                        radiusHolder = Convert.ToInt32(split[1]);//coverts users text to radius variable
                    }
                    else
                    {
                        int radiusInc = Convert.ToInt32(split[4]);//else adding to radius
                        radiusHolder = radiusHolder + radiusInc;
                    }
                }
                else if (split[0].Equals("width") == true)  //same as clear button resets to blank canvass
                {
                    int num1 = Convert.ToInt32(split[1]);
                    widthHolder = num1;
                }
                else if (split[0].Equals("height") == true)  //same as clear button resets to blank canvass
                {
                    int num1 = Convert.ToInt32(split[1]);
                    heightHolder = num1;
                }
                else
                {
                    // MyCanvass.PrintError(2, lNum);//incorrect commnad
                }
                commandLine.Text = "";
                Refresh();
            }
            catch (System.FormatException)  // catches number format exception e.g float or letters
            {
                // MyCanvass.PrintError(5, 0);
                Refresh();
            }


        }

        private void Clear_Click(object sender, EventArgs e)//clears canvass and resets xPos yPos
        {
            MyCanvass.Clear();
            Refresh();

        }

        private void Syntax_Click(object sender, EventArgs e)//sends lines to CheckingSyntax to be checked for errors
        {
            /*
             * is exactly the same as run() where you will find all comments for this methods
             * this method just passes the commands to be checked in checkingSyntax rather
             * than runiing them in parseCommand
             */
            int t;
            int i;
            if (richTextBox1.Text != "")
            {
                for (i = 0; i < richTextBox1.Lines.Length; i++)
                {
                    String Line = richTextBox1.Lines[i];
                    String Command = Line.Trim(); //removes excess spaces from the ends ONLY
                    string[] split = Command.Split(' ');


                    if (split[0].Equals("If"))
                    {
                        int num1 = Convert.ToInt32(split[3]);
                        if (split[1].Equals("var"))
                        {
                            if (split[2].Equals("==") == true)
                            {
                                if (var == num1)
                                {
                                    IfEx = true;
                                }
                                else
                                {
                                    IfEx = false;
                                }

                            }
                            else if (split[2].Equals("!=") == true)
                            {
                                if (var != num1)
                                {
                                    IfEx = true;
                                }
                                else
                                {
                                    IfEx = false;
                                }

                            }
                            else if (split[2].Equals("<") == true)
                            {
                                if (var < num1)
                                {
                                    IfEx = true;
                                }
                                else
                                {
                                    IfEx = false;
                                }

                            }
                            else if (split[2].Equals(">=") == true)
                            {
                                if (var >= num1)
                                {
                                    IfEx = true;
                                }
                                else
                                {
                                    IfEx = false;
                                }

                            }
                            else
                            {
                                MyCanvass.PrintError(8, i);
                                Refresh();
                                IfEx = false;
                            }
                            try
                            {
                                if (FormExcep == false && IfEx == true)
                                {
                                    for (t = (i); t < (richTextBox1.Lines.Length + 1); t++)
                                    {
                                        String Line1 = richTextBox1.Lines[t];
                                        if (Line1 != "endif")
                                        {
                                            CheckingSyntax(Line1, t);
                                        }
                                        else
                                        {
                                            Console.WriteLine("ENDIF");
                                            i = t;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    for (t = (i); t < (richTextBox1.Lines.Length + 1); t++)
                                    {
                                        String Line1 = richTextBox1.Lines[t];
                                        if (Line1 == "endif")
                                        {
                                            Console.WriteLine("ENDIF");
                                            i = t;
                                            break;
                                        }
                                    }
                                }
                            }
                            catch (System.IndexOutOfRangeException)
                            {
                                MyCanvass.PrintError(9, i);
                                Refresh();
                            }
                        }
                    }
                    else if (split[0].Equals("loop") == true)
                    {
                        for (int checkLoop = i + 1; checkLoop < richTextBox1.Lines.Length; checkLoop++)
                        {
                            String Line1 = richTextBox1.Lines[checkLoop];
                            if (Line1 == "endloop")
                            {
                                endLoopLineNumber = checkLoop;
                                break;
                            }
                        }
                        try
                        {
                            loopHolder = Convert.ToInt32(split[1]);
                        }
                        catch (System.FormatException)
                        {
                            MyCanvass.PrintError(10, i);
                            Refresh();
                        }
                        catch (System.IndexOutOfRangeException)
                        {
                            MyCanvass.PrintError(10, i);
                            Refresh();
                        }
                        for (int v = 0; v <= loopHolder; v++)
                        {
                            int startLoop = (i + 1);
                            for (int p = startLoop; p < endLoopLineNumber; p++)
                            {
                                String Line1 = richTextBox1.Lines[p];
                                CheckingSyntax(Line1, p);
                            }
                        }
                        i = (endLoopLineNumber);
                    }
                    else if (split[0].Equals("method") == true)
                    {
                        int checkLoop;
                        int sizeOfArray;
                        methodStart = i + 1;
                        for (checkLoop = i + 1; checkLoop < richTextBox1.Lines.Length; checkLoop++)
                        {
                            String Line1 = richTextBox1.Lines[checkLoop];
                            if (Line1 == "endmethod")
                            {
                                endMethodLineNumber = checkLoop;
                                break;
                            }
                        }
                        methodName = split[1];
                        sizeOfArray = (endMethodLineNumber - i);
                        commandArray = new string[sizeOfArray];
                        int c = 0;
                        for (int v = i + 1; v < endMethodLineNumber; v++)
                        {
                            String Line1 = richTextBox1.Lines[v];
                            methodHandling(Line1, c);
                            c++;
                        }
                        i = endMethodLineNumber + 1;
                    }
                    else if (split[0].Equals(methodName) == true)
                    {
                        checkMethodExecuting(i);
                    }
                    else
                    {
                        CheckingSyntax(Line, i);
                    }
                }
            }
            else
            {
                MyCanvass.PrintError(1, 0);//text area is empty
                Refresh();
            }
        }
        private void CheckingSyntax(String Line, int lNum)//runs the same code as parseCommand without running the commnands, instead checking for errors
        {
            try
            {
                String Command = Line.Trim();
                string[] split = Command.Split(' ');
                if (split[0].Equals("drawTo") == true)
                {
                    int num1 = Convert.ToInt32(split[1]);
                    int num2 = Convert.ToInt32(split[2]);

                    if ((num1 > 0 && num1 < 640) && (num2 > 0 && num2 < 640))
                    {

                        Console.WriteLine("LINE");
                    }
                    else
                    {
                        MyCanvass.PrintError(6, lNum);//too big for canvass
                    }
                }
                else if (split[0].Equals("square") == true)
                {
                    int num1 = Convert.ToInt32(split[1]);
                    if (num1 > 0 && num1 < 640)
                    {

                        Console.WriteLine("SQUARE");
                    }
                    else
                    {
                        MyCanvass.PrintError(6, lNum);//too big for canvass
                    }

                }
                else if (split[0].Equals("rectangle") == true)
                {
                    if (split.Length == 1)
                    {
                        int num1 = Convert.ToInt32(split[1]);    //converts initial string value of number to an int
                        int num2 = Convert.ToInt32(split[2]);

                        if ((num1 > 0 && num1 < 640) && (num2 > 0 && num2 < 640))   //makes sure the numbers are appropriate for the size of the canvass 0 - 640
                        {
                           // MyCanvass.DrawRectangle(num1, num2);    //draws rectangle width, height around xPos yPos
                            Console.WriteLine("RECTANGLE");
                        }
                        else
                        {
                            MyCanvass.PrintError(6, lNum);
                            Refresh();
                        }
                    }
                    else
                    {
                        if (widthHolder == 0 || heightHolder == 0)
                        {
                            MyCanvass.PrintError(11, lNum);
                            Refresh();
                        }
                        else
                        {
                            //MyCanvass.DrawRectangle(widthHolder, heightHolder);    //draws rectangle width, height around xPos yPos
                            Console.WriteLine("RECTANGLE");
                        }
                    }
                }
                else if (split[0].Equals("circle") == true)
                {
                    try
                    {
                        if (split[1].Equals("var") == true)
                        {
                            if (var == 0)
                            {
                                MyCanvass.PrintError(13, lNum);//var needs to be set
                                Refresh();
                            }
                            else
                            {
                                //MyCanvass.DrawCircle(var);
                            }
                        }
                        else if (split[1].Equals("radius") == true)
                        {
                            if (radiusHolder == 0)
                            {
                                MyCanvass.PrintError(12, lNum);//radius needs to be set
                                Refresh();                                
                            }
                            else
                            {
                                //MyCanvass.DrawCircle(radiusHolder);
                            }
                        }
                        else
                        {
                            int num1 = Convert.ToInt32(split[1]);   //converts initial string value of number to an int

                            if (num1 > 0 && num1 < 640) //makes sure the numbers are appropriate for the size of the canvass 0 - 640
                            {
                                //MyCanvass.DrawCircle(num1);  //draws circle with radius - num1-  around xPos yPos
                                Console.WriteLine("CIRCLE");
                            }
                            else
                            {
                                MyCanvass.PrintError(6, lNum);
                            }
                        }
                    }
                    catch (System.IndexOutOfRangeException)
                    {
                        MyCanvass.PrintError(2, lNum);//no element in that array
                        Refresh();
                    }
                    catch (System.FormatException)
                    {
                        MyCanvass.PrintError(5, lNum);//no number on that line
                        Refresh();
                    }
                }
                else if (split[0].Equals("triangle") == true)
                {
                    try
                    {
                        if (split[1].Equals("var") == true) //can draw triangle with a variable set before hand
                        {
                            if (var == 0)
                            {
                                MyCanvass.PrintError(13, lNum);//if not set, show error
                                Refresh();
                            }
                            else
                            {
                                //MyCanvass.DrawTriangle(var);
                            }
                        }
                        else
                        {
                            int num1 = Convert.ToInt32(split[1]);

                            if (num1 > 0 && num1 < 640) //canvass range
                            {
                                //MyCanvass.DrawTriangle(num1);    //draws triangle of size - num1 - around xpos yPos
                                Console.WriteLine("TRIANGLE");
                            }
                            else
                            {
                                MyCanvass.PrintError(6, lNum);//too big for canvass
                            }
                        }
                    }
                    catch (System.IndexOutOfRangeException)
                    {
                        MyCanvass.PrintError(2, lNum);
                        Refresh();
                    }
                    catch (System.FormatException)
                    {
                        MyCanvass.PrintError(5, lNum);
                        Refresh();
                    }
                }
                else if (split[0].Equals("moveTo") == true)
                {
                    int num1 = Convert.ToInt32(split[1]);
                    int num2 = Convert.ToInt32(split[2]);

                    if ((num1 > 0 && num1 < 640) && (num2 > 0 && num2 < 640))
                    {

                        Console.WriteLine("Pen Moved");
                    }
                    else
                    {
                        MyCanvass.PrintError(6, lNum);//too big for canvass
                    }
                }
                else if (split[0].Equals("clear") == true)
                {

                }
                else if (split[0].Equals("reset") == true)
                {

                    Console.WriteLine("Pen Moved");
                }
                else if (split[0].Equals("pen") == true)
                {
                    String color = split[1];
                    if (color == "red" || color == "green" || color == "yellow" || color == "black")
                    {
                        Console.WriteLine("PEN COLOR CHANGED TO: ", color);
                    }
                    else
                    {
                        MyCanvass.PrintError(3, lNum); //incorrect pen color
                    }
                }
                else if (split.Length >= 6)
                {
                    MyCanvass.PrintError(0, lNum);//Too many parameters 
                }
                else if (split[0].Equals("fill") == true)
                {
                    if (split[1].Equals("y") == true)
                    {

                    }
                    else if (split[1].Equals("n") == true)
                    {

                    }
                    else
                    {
                        MyCanvass.PrintError(4, lNum);//invalid fill option
                    }
                }
                else if (split[0].Equals("radius") == true)  //same as clear button resets to blank canvass
                {
                    if (split.Length == 2)
                    {
                        radiusHolder = Convert.ToInt32(split[1]);
                    }
                    else
                    {
                        int radiusInc = Convert.ToInt32(split[4]);
                        radiusHolder = radiusHolder + radiusInc;
                    }
                }
                else if (split[0].Equals("var") == true)   //allows for the user to have the fill option on or off (y/n)
                {
                    if (split.Length == 2)
                    {
                        var = Convert.ToInt32(split[1]);
                    }
                    else
                    {
                        int varInc = Convert.ToInt32(split[4]);
                        var = var + varInc;
                    }
                }
                else
                {
                    MyCanvass.PrintError(2, lNum);//incorrect commnad
                }
                commandLine.Text = "";
                Refresh();
            }
            catch (System.FormatException)
            {
                MyCanvass.PrintError(5, lNum);//number invalid
                Refresh();
            }
        }

        private void reset_Click(object sender, EventArgs e)//resets pen to (0,0) wihtout clearing canvass
        {
            MyCanvass.moveTo(0, 0);
            Refresh();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stream myStream;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if ((myStream = openFileDialog1.OpenFile()) != null)
                {
                    String fileName = openFileDialog1.FileName;
                    String fileText = File.ReadAllText(fileName);
                    richTextBox1.Text = fileText;
                    myStream.Close();
                }
            }


        }//opens file to load into richtextBox

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var sfd = new SaveFileDialog())
            {
                sfd.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                sfd.FilterIndex = 2;
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    File.WriteAllText(sfd.FileName, richTextBox1.Text);
                }
            }
            MyCanvass.Clear();
            Refresh();
            richTextBox1.Text = "";
        }//saves file from richtextBox to .txt file

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MyCanvass.About();
            Refresh();
        }//about the programme

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }//exit button

        /*
         * The following method takes each line from the text box analyses it
         * and then passes it to the correct method to be processed onto the canvass         * 
         */
        private void run()//passes commands to parseCommand 
        {
            int t;
            int i;
            if (richTextBox1.Text != "")
            {
                for (i = 0; i < richTextBox1.Lines.Length; i++)
                {
                    String Line = richTextBox1.Lines[i];
                    String Command = Line.Trim(); //removes excess spaces from the ends ONLY
                    string[] split = Command.Split(' ');

                    if (split[0].Equals("If"))//if statement entered
                    {                       
                        try
                        {
                            int num1 = Convert.ToInt32(split[3]);
                        
                        if (split[1].Equals("var"))
                        {
                            if (split[2].Equals("==") == true) //checks of values are equal
                            {
                                if (var == num1)
                                {
                                    IfEx = true;    //if they are equal then execute code
                                }
                                else
                                {
                                    IfEx = false;   //if they are not equal then dont execute code
                                }

                            }
                            else if (split[2].Equals("!=") == true)// checks of values are not equal
                            {
                                if (var != num1)
                                {
                                    IfEx = true;
                                }
                                else
                                {
                                    IfEx = false;
                                }

                            }
                            else if (split[2].Equals("<") == true)// checks of values are less than equal
                                {
                                if (var < num1)
                                {
                                    IfEx = true;
                                }
                                else
                                {
                                    IfEx = false;
                                }

                            }
                            else if (split[2].Equals(">=") == true)// checks of values are greater than or equal
                                {
                                if (var >= num1)
                                {
                                    IfEx = true;
                                }
                                else
                                {
                                    IfEx = false;
                                }

                            }
                            else
                            {
                                IfEx = false; //if nothing then dont execute code
                            }
                            if (FormExcep == false && IfEx == true)//checks if there has been no format excepotion and if we are allowed to execute
                            {
                                for (t = (i); t < (richTextBox1.Lines.Length + 1); t++)
                                {
                                    String Line1 = richTextBox1.Lines[t];
                                    if (Line1 != "endif")//executes as long as it is not the end of the ifstatement
                                    {
                                        parseCommand(Line1, 0);//executes code
                                    }
                                    else
                                    {
                                        Console.WriteLine("ENDIF");
                                        i = t;
                                        break;//set the line back to the end of the if statement and break
                                    }
                                }
                            }
                            else//still runs through the code to find out where the if statemnt ends 
                            {
                                for (t = (i); t < (richTextBox1.Lines.Length + 1); t++)
                                {
                                    String Line1 = richTextBox1.Lines[t];
                                    if (Line1 == "endif")
                                    {
                                        Console.WriteLine("ENDIF");
                                        i = t;
                                        break;
                                    }
                                }
                            }
                        }
                        }
                        catch (System.FormatException)
                        {
                            //MyCanvass.PrintError(5, i);//incorrect number 
                           // Refresh();                            
                        }
                    }
                    else if (split[0].Equals("loop") == true)
                    {
                        for (int checkLoop = i + 1; checkLoop < richTextBox1.Lines.Length; checkLoop++)//need to go through loop once to find out where endloop is
                        {
                            String Line1 = richTextBox1.Lines[checkLoop];
                            if (Line1 == "endloop")
                            {
                                endLoopLineNumber = checkLoop;//holds the line number for the end of the loop
                                break;
                            }
                        }
                        loopHolder = Convert.ToInt32(split[1]);
                        for (int v = 0; v <= loopHolder; v++)//the amount of times we loop
                        {
                            int startLoop = (i + 1);
                            for (int p = startLoop; p < endLoopLineNumber; p++)//executing each line of code in the loop
                            {
                                String Line1 = richTextBox1.Lines[p];
                                parseCommand(Line1, 0);
                            }
                        }
                        i = (endLoopLineNumber);//resets i to the line after the loop
                    }
                    else if (split[0].Equals("method") == true)
                    {
                        int checkLoop;
                        int sizeOfArray;
                        for (checkLoop = i + 1; checkLoop < richTextBox1.Lines.Length; checkLoop++)
                        {
                            String Line1 = richTextBox1.Lines[checkLoop];
                            if (Line1 == "endmethod")//find the line number of endmethod
                            {
                                endMethodLineNumber = checkLoop;
                                break;
                            }
                        }
                        methodName = split[1];//store method name
                        sizeOfArray = (endMethodLineNumber - i);//creates array size using line numbers
                        commandArray = new string[sizeOfArray];//creates array
                        int c = 0;
                        for (int v = i + 1; v < endMethodLineNumber; v++)//store code into array
                        {
                            String Line1 = richTextBox1.Lines[v];
                            methodHandling(Line1, c);//with element in array number
                            c++;
                        }
                        i = endMethodLineNumber;
                    }
                    else if (split[0].Equals(methodName) == true)
                    {
                        methodExecuting();//executes method when called
                    }
                    else
                    {
                        parseCommand(Line, i);
                    }
                }
            }
            else
            {
                MyCanvass.PrintError(1, 0);//text area is empty
                Refresh();
            }
        }
        private void methodHandling(String Line, int c)
        {
            commandArray[c] = Line;//filling method array
        }
        private void methodExecuting()
        {
            for (int i = 0; i < (commandArray.Length - 1); i++)
            {
                parseCommand(commandArray[i], 0);//executes each command in the array
            }
        }
        private void checkMethodExecuting(int p)
        {
            for (int i = 0; i < (commandArray.Length - 1); i++)
            {
                CheckingSyntax(commandArray[i], methodStart+i);//passing line number also 
            }
        }
    }
}
