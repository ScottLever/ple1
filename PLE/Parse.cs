﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLE
{
    public class Parse
    {
        /*
         * Example class to show that I understand the unit testing
         * Didnt work for my Form1 class or Canvass class 
         * Because System.Windows.Forms was invalid... 
         */
        Boolean outOfBounds = false;
        Boolean invalidNumber = false;
        public int SquareParse(String com, int lNum)
        {
            int result = 0;

            String Command = com.Trim().ToLower();
            string[] split = Command.Split(' ');
            if (split[0].Equals("square") == true)
            {
                int num1 = Convert.ToInt32(split[1]);
                result = num1;

            }
            return result;

        }
        public int MoveToParse(String com, int lNum)
        {
            int result = 0;

            String Command = com.Trim();
            string[] split = Command.Split(' ');
            if (split[0].Equals("moveTo") == true)
            {
                int num1 = Convert.ToInt32(split[1]);
                int num2 = Convert.ToInt32(split[2]);

                result = (num1 + num2);
            }
            return result;

        }
        public Boolean fill(String s)
        {
            Boolean fill = false;
            string[] split = s.Split(' ');
            String p = split[1];
            if(p == "y")
            {
                fill = true;
            }else if (p == "n")
            {
                fill = false;
            }
            return fill;
        }
        public Boolean TriangleParse(String com, int lNum)
        {
            String Command = com.Trim().ToLower();
            string[] split = Command.Split(' ');
            if (split[0].Equals("triangle") == true)
            {
                int num1 = Convert.ToInt32(split[1]);
                if ((num1 > 0 && num1 < 640)) //makes sure the numbers are appropriate for the size of the canvass 0 - 640
                {
                    outOfBounds = false;
                }
                else
                {
                    outOfBounds = true;
                }

            }
            return outOfBounds;

        }
        public Boolean RectangleParse(String com, int lNum)
        {
            try
            {
                String Command = com.Trim().ToLower();
                string[] split = Command.Split(' ');
                if (split[0].Equals("rectangle") == true)
                {
                    int num1 = Convert.ToInt32(split[1]);
                    int num2 = Convert.ToInt32(split[2]);
                    invalidNumber = false;

                }
            }
            catch (System.FormatException)
            {
                invalidNumber = true;


            }
            return invalidNumber;

        }
        public int Value(String s)
        {
            int value1;
            string[] split = s.Split(' ');
            value1 = Convert.ToInt32(split[1]);
            return value1;
        }
        public string Operator(String s)
        {
            string[] split = s.Split(' ');
            String p = split[2];
            return p;
        }
        public Boolean Loop(String s)
        {
            Boolean paramIsNumber;
            try
            {
                int i;
                string[] split = s.Split(' ');
                String p = split[1];
                i = Convert.ToInt32(p);
                paramIsNumber = true;
            }
            catch (System.FormatException)
            {
                paramIsNumber = false;
            }
            return paramIsNumber;
        }
        public Boolean Loop2(String s)
        {
            Boolean paramIsNumber;
            try
            {
                int i;
                string[] split = s.Split(' ');
                String p = split[1];
                i = Convert.ToInt32(p);
                paramIsNumber = true;
            }
            catch (System.IndexOutOfRangeException)
            {
                paramIsNumber = false;
            }
            return paramIsNumber;
        }
        public string method(String s)
        {
            string[] split = s.Split(' ');
            String p = split[1];
            return p;
        }
    }
    
}
