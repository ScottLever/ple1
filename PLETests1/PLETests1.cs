using PLE;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.ComponentModel.DataAnnotations;

namespace PLETests1
{
    [TestClass]
    public class PLETests
    {
        /*
         * A test to see if the value for the side length of the 
         * square is correctly parsed through;
         */
        [TestMethod]
        public void Square_Parameter_Is_Correct()
        {
            Parse p = new Parse();
            int actual = p.SquareParse("square 100", 0);
            int expected = 100;
            Assert.AreEqual(expected, actual);
        }
        /*
         * A test to determine if the moveTo coordinates are
         * parsed correctly
         */
        [TestMethod]
        public void MoveTo_Parameters_Correct()
        {
            Parse p = new Parse();
            int actual = p.MoveToParse("moveTo 160 200", 0);
            int expected = 360;
            Assert.AreEqual(expected, actual);
        }
        /*
         * A test to determine if the fill command is working correctly
         * by passing 'fill y' and expecting the boolean to return true - 
         * as in the shape has been filled
         */
        [TestMethod]
        public void Fill_y_n()
        {
            Parse p = new Parse();
            var actual = p.fill("fill y");
            var expected = true;
            Assert.AreEqual(expected, actual);
        }
        /*
         * A test to see if the out of bounds exception is working properly
         * within my code, the upper boundry being 640 and we are passing 
         * 700 through
         */
        [TestMethod]
        public void Triangle_OutOfBounds_Parameters()
        {
            Parse p = new Parse();
            var actual = p.TriangleParse("triangle 700", 0);
            var expected = true;
            Assert.AreEqual(expected, actual);
        }
        /*
         * A test to see if my exception handling for an invalid number
         * is done correctly
         */
        [TestMethod]
        public void Rectangle_Invalid_Number_Exception()
        {
            Parse p = new Parse();
            var actual = p.RectangleParse("rectangle 100 f", 0);
            var expected = true;
            Assert.AreEqual(expected, actual);
        }
        /*
         * A test to see if the var value inputted by the user is 
         * passed correctly through the program
         */
        [TestMethod]
        public void testVar()
        {
            Parse p = new Parse();
            int actual = p.Value("var 20");
            int expected = 20;
            Assert.AreEqual(expected, actual);
        }
        /*
         * A test to see if I am checking the operation I should
         * be performing when comparing the two variables that allow the
         * if statement to be entered
         */
        [TestMethod]
        public void If_Operator_Splits_Correctly()
        {
            Parse p = new Parse();
            string actual = p.Operator("If var != 20");
            string expected = "!=";
            Assert.AreEqual(expected, actual);
        }
        /*
         * A test to see if the number format exception is working
         * corrrectly within the code, you cannont loop 'asd' amount of times
         * it should be a number or a variable
         * 
         * expecting an error in the input here
         */
        [TestMethod]
        public void Loop_Format_Exception()
        {
            Parse p = new Parse();
            var actual = p.Loop("loop asd");
            var expected = false;
            Assert.AreEqual(expected, actual);
        }
        /*
         * A test to see if the out of bounds exception is working correctly
         * there should be two elements in the loop array, it throws an exception if not
         * and reports error
         * 
         * expecting an error in the input here
         */
        [TestMethod]
        public void Loop_Index_Out_Of_Range_Exception()
        {
            Parse p = new Parse();
            var actual = p.Loop2("loop");
            var expected = false;
            Assert.AreEqual(expected, actual);
        }
        /*
         * A test to see if the method name is saved correctly
         * into a string and therefore can be called anywhere throughout
         * the program by matching any line to the method name
         */
        [TestMethod]
        public void Method_Name_Called_Anywhere()
        {
            Parse p = new Parse();
            string actual = p.method("method firstMethod()");
            string expected = "firstMethod()";
            Assert.AreEqual(expected, actual);
        }
    }
}

